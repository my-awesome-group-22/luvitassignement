import 'package:flutter/material.dart';
import 'package:luvit_assesment/cards/first_card.dart';
import 'package:luvit_assesment/cards/forth_card.dart';
import 'package:luvit_assesment/cards/second_card.dart';
import 'package:luvit_assesment/cards/third_card.dart';
import 'package:luvit_assesment/utils/constants.dart';
import 'package:sizer/sizer.dart';

class Homepage extends StatefulWidget {
  const Homepage({super.key});

  @override
  State<Homepage> createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  List<String> UserDataList = [
    "assets/image1.png",
    "assets/image3.png",
    "assets/image2.png",
    ""
  ];
  int index = 0;

  final scrollController = ScrollController();
  List<PageController> pageControllers = [];
  late int _currentIndex;

  @override
  void initState() {
    _currentIndex = 0;

    super.initState();
    pageControllers = List.generate(
        UserDataList.length,
        (index) => PageController(
              viewportFraction: 0.94, // Adjust the fraction as needed
            ));
    scrollController.addListener(() {
      for (var pageController in pageControllers) {
        pageController.jumpToPage(0);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.black,
      appBar: AppBar(
        actions: [
          Container(
            width: 30.w,
            padding: EdgeInsets.symmetric(vertical: 0.w, horizontal: 0.5.w),
            margin: EdgeInsets.symmetric(vertical: 1.5.w, horizontal: 0.5.w),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(8.w)),
                border: Border.all(color: CustomColors.secondgrey)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  "assets/st1.png",
                  width: 30,
                  height: 48,
                ),
                Text("323,233",
                    style: TextStyle(
                      fontSize: 12.sp,
                      color: CustomColors.lightWhite,
                      fontWeight: FontWeight.w500,
                    ))
              ],
            ),
          ),
          Image.asset(
            "assets/bell.png",
            width: 48,
            height: 48,
          ),
        ],
        leading: Image.asset(
          "assets/loc.png",
          width: 48,
          height: 48,
        ),
        backgroundColor: CustomColors.black,
        leadingWidth: 6.w,
        centerTitle: false,
        title: Text(
          '목이길어슬픈기린님의 새로운 스팟',
          style: TextStyle(
            fontSize: 10.sp,
            color: CustomColors.white,
          ),
        ),
      ),
      body: SizedBox(
        child: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(4.w))),
              margin: EdgeInsets.symmetric(vertical: 2.w, horizontal: 3.w),
              height: 68.2.h,
              width: 100.w,
              padding: EdgeInsets.all(0.w),
              child: PageView.builder(
                controller: pageControllers[index],
                itemCount: UserDataList.length,
                onPageChanged: (index) {
                  setState(() {
                    _currentIndex = index;
                  });
                },
                itemBuilder: (context, secindex) {
                  return Container(
                    child: Stack(
                      children: [
                        secindex != 3
                            ? ClipRRect(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(6.w)),
                                child: Image.asset(
                                  UserDataList[secindex],
                                  // fit: BoxFit.cover,
                                  width: 84.w,
                                  height: 85.h,
                                ),
                              )
                            : const SizedBox(),
                        secindex == 0
                            ? FirstCard(fruit: UserDataList[index])
                            : secindex == 1
                                ? SecondCard(fruit: UserDataList[index])
                                : secindex == 2
                                    ? ThirdCard(fruit: UserDataList[index])
                                    : const ForthCard(),
                      ],
                    ),
                  );
                },
              ),
            ),
            SizedBox(height: 5.w),
            Column(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 7.w),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: List.generate(
                      UserDataList.length,
                      (imageindex) => Container(
                        margin: const EdgeInsets.symmetric(horizontal: 4),
                        width: 16.w,
                        height: 1.w,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(2.w)),
                          shape: BoxShape.rectangle,
                          color: _currentIndex == imageindex
                              ? Colors.red
                              : Colors.grey,
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: double.infinity,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      // Your existing Stack content...
                      GestureDetector(
                        child: Container(
                          color: Colors.transparent,
                          height: 20.w,
                          width: 30.w,
                        ),
                        onTap: () {
                          if (_currentIndex > 0) {
                            setState(() {
                              _currentIndex--;
                            });
                            for (var pageController in pageControllers) {
                              if (pageController.hasClients) {
                                pageController.previousPage(
                                    duration: const Duration(milliseconds: 300),
                                    curve: Curves.ease);
                              }
                            }
                          }
                        },
                      ),
                      GestureDetector(
                        child: Container(
                          color: Colors.transparent,
                          height: 20.w,
                          width: 30.w,
                        ),
                        onTap: () {
                          if (_currentIndex < UserDataList.length - 1) {
                            setState(() {
                              _currentIndex++;
                            });
                            for (var pageController in pageControllers) {
                              if (pageController.hasClients) {
                                pageController.nextPage(
                                    duration: const Duration(milliseconds: 300),
                                    curve: Curves.ease);
                              }
                            }
                          }
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
