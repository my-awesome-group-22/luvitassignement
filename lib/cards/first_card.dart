import 'package:flutter/material.dart';
import 'package:luvit_assesment/utils/constants.dart';
import 'package:sizer/sizer.dart';

class FirstCard extends StatelessWidget {
  const FirstCard({
    super.key,
    required this.fruit,
  });

  final String fruit;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 85.w,
      height: 80.h,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8.w)),
          border: Border.all(color: CustomColors.secondgrey)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            height: 25.h,
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.bottomCenter,
                end: Alignment.topCenter,
                colors: [CustomColors.black, Colors.transparent],
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: 25.w,
                          padding: EdgeInsets.symmetric(
                              vertical: 0.w, horizontal: 0.5.w),
                          margin: EdgeInsets.symmetric(
                              vertical: 1.5.w, horizontal: 0.5.w),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8.w)),
                              color: CustomColors.black,
                              border:
                                  Border.all(color: CustomColors.secondgrey)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Icon(
                                Icons.star_rounded,
                                size: 7.w,
                                color: const Color.fromARGB(255, 46, 43, 43),
                              ),
                              Text("29,930",
                                  style: TextStyle(
                                    color: CustomColors.lightWhite,
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w500,
                                  ))
                            ],
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Text("잭과분홍콩나물",
                                    style: TextStyle(
                                      fontSize: 16.sp,
                                      color: CustomColors.white,
                                      fontWeight: FontWeight.w700,
                                    )),
                                const Text(" 25",
                                    style: TextStyle(
                                      fontSize: 24,
                                      color: CustomColors.lightWhite,
                                      fontWeight: FontWeight.w300,
                                    ))
                              ],
                            ),
                            const Row(
                              children: [
                                Text("서울",
                                    style: TextStyle(
                                      color: CustomColors.white,
                                      fontSize: 15,
                                      fontWeight: FontWeight.w300,
                                    )),
                                Text("·",
                                    style: TextStyle(
                                      color: CustomColors.white,
                                      fontSize: 15,
                                      fontWeight: FontWeight.w300,
                                    )),
                                Text("2km 거리에 있음",
                                    style: TextStyle(
                                      color: CustomColors.white,
                                      fontSize: 15,
                                      fontWeight: FontWeight.w300,
                                    ))
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          "assets/btcon_48.png",
                          width: 48,
                          height: 48,
                        ),
                        //
                      ],
                    )
                  ],
                ),
                SizedBox(
                  height: 10.w,
                ),
                Icon(
                  Icons.keyboard_arrow_down,
                  size: 6.w,
                  color: CustomColors.lightWhite,
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
