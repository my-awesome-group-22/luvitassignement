import 'package:flutter/material.dart';
import 'package:luvit_assesment/utils/constants.dart';
import 'package:sizer/sizer.dart';

class ThirdCard extends StatelessWidget {
  const ThirdCard({
    super.key,
    required this.fruit,
  });

  final String fruit;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 85.w,
      height: 85.h,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(6.w)),
          border: Border.all(color: CustomColors.secondgrey)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            height: 35.h,
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.bottomCenter,
                end: Alignment.topCenter,
                colors: [CustomColors.black, Colors.transparent],
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: 25.w,
                          padding: EdgeInsets.symmetric(
                              vertical: 0.w, horizontal: 0.5.w),
                          margin: EdgeInsets.symmetric(
                              vertical: 1.5.w, horizontal: 0.5.w),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8.w)),
                              color: CustomColors.black,
                              border:
                                  Border.all(color: CustomColors.secondgrey)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Icon(
                                Icons.star_rounded,
                                size: 7.w,
                                color: const Color.fromARGB(255, 46, 43, 43),
                              ),
                              Text("29,930",
                                  style: TextStyle(
                                    color: CustomColors.lightWhite,
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w500,
                                  ))
                            ],
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Text("잭과분홍콩나물",
                                    style: TextStyle(
                                      fontSize: 16.sp,
                                      color: CustomColors.white,
                                      fontWeight: FontWeight.w700,
                                    )),
                                const Text(" 25",
                                    style: TextStyle(
                                      fontSize: 24,
                                      color: CustomColors.lightWhite,
                                      fontWeight: FontWeight.w300,
                                    ))
                              ],
                            ),
                            SizedBox(
                              width: 60.w,
                              child: Wrap(children: [
                                Container(
                                  alignment: Alignment.center,
                                  width: 30.w,
                                  padding: EdgeInsets.symmetric(
                                      vertical: 1.w, horizontal: 0.5.w),
                                  margin: EdgeInsets.symmetric(
                                      vertical: 1.w, horizontal: 0.5.w),
                                  decoration: BoxDecoration(
                                      color: CustomColors.black,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(8.w)),
                                      border: Border.all(
                                          color: CustomColors.secondgrey)),
                                  child: Text("🍸 전혀 안 함",
                                      style: TextStyle(
                                        fontSize: 12.sp,
                                        color: CustomColors.white,
                                        fontWeight: FontWeight.w500,
                                      )),
                                ),
                                Container(
                                  alignment: Alignment.center,
                                  width: 25.w,
                                  padding: EdgeInsets.symmetric(
                                      vertical: 1.w, horizontal: 0.5.w),
                                  margin: EdgeInsets.symmetric(
                                      vertical: 1.w, horizontal: 0.5.w),
                                  decoration: BoxDecoration(
                                      color: CustomColors.black,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(8.w)),
                                      border: Border.all(
                                          color: CustomColors.secondgrey)),
                                  child: Text("🚬 비흡연",
                                      style: TextStyle(
                                        fontSize: 12.sp,
                                        color: CustomColors.white,
                                        fontWeight: FontWeight.w500,
                                      )),
                                ),
                                Container(
                                  alignment: Alignment.center,
                                  width: 40.w,
                                  padding: EdgeInsets.symmetric(
                                      vertical: 1.w, horizontal: 0.5.w),
                                  margin: EdgeInsets.symmetric(
                                      vertical: 1.w, horizontal: 0.5.w),
                                  decoration: BoxDecoration(
                                      color: CustomColors.black,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(8.w)),
                                      border: Border.all(
                                          color: CustomColors.secondgrey)),
                                  child: Text("💪🏻 매일 1시간 이상",
                                      style: TextStyle(
                                        fontSize: 12.sp,
                                        color: CustomColors.white,
                                        fontWeight: FontWeight.w500,
                                      )),
                                ),
                                Container(
                                  alignment: Alignment.center,
                                  width: 42.w,
                                  padding: EdgeInsets.symmetric(
                                      vertical: 1.w, horizontal: 0.5.w),
                                  margin: EdgeInsets.symmetric(
                                      vertical: 1.w, horizontal: 0.5.w),
                                  decoration: BoxDecoration(
                                      color: CustomColors.black,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(8.w)),
                                      border: Border.all(
                                          color: CustomColors.secondgrey)),
                                  child: Text("🤝 만나는 걸 좋아함",
                                      style: TextStyle(
                                        fontSize: 12.sp,
                                        color: CustomColors.white,
                                        fontWeight: FontWeight.w500,
                                      )),
                                ),
                                Container(
                                  alignment: Alignment.center,
                                  width: 15.w,
                                  padding: EdgeInsets.symmetric(
                                      vertical: 1.w, horizontal: 0.5.w),
                                  margin: EdgeInsets.symmetric(
                                      vertical: 1.w, horizontal: 0.5.w),
                                  decoration: BoxDecoration(
                                      color: CustomColors.black,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(8.w)),
                                      border: Border.all(
                                          color: CustomColors.secondgrey)),
                                  child: Text("INFP",
                                      style: TextStyle(
                                        fontSize: 12.sp,
                                        color: CustomColors.white,
                                        fontWeight: FontWeight.w500,
                                      )),
                                ),
                              ]),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          "assets/btcon_48.png",
                          width: 48,
                          height: 48,
                        ),
                        //
                      ],
                    )
                  ],
                ),
                SizedBox(
                  height: 10.w,
                ),
                Icon(
                  Icons.keyboard_arrow_down,
                  size: 6.w,
                  color: CustomColors.lightWhite,
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
