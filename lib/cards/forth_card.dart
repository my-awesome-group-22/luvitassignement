import 'package:flutter/material.dart';
import 'package:luvit_assesment/utils/constants.dart';
import 'package:sizer/sizer.dart';

class ForthCard extends StatelessWidget {
  const ForthCard({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 90.w,
      height: 85.h,
      decoration: BoxDecoration(
          color: CustomColors.black,
          borderRadius: BorderRadius.all(Radius.circular(6.w)),
          border: Border.all(color: CustomColors.secondgrey)),
      child: const Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("잭과분홍콩나물",
              style: TextStyle(
                fontSize: 28,
                fontWeight: FontWeight.w700,
                color: CustomColors.lightWhite,
              )),
          Text("잭과분홍콩나물",
              style: TextStyle(
                fontSize: 28,
                color: CustomColors.lightWhite,
                fontWeight: FontWeight.w700,
              )),
        ],
      ),
    );
  }
}
