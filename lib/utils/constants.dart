import 'dart:ui';

class CustomColors {
  ///primary colors
  static const white = Color(0xffffffff);
  static const lightWhite = Color(0xffF5F5F5);
  static const black = Color(0xff000000);
  static const red = Color(0xffFF016B);
  static const secondgrey = Color(0xff3A3A3A);
  static const secondblack = Color(0xff232327);
}
