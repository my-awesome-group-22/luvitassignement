import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:luvit_assesment/utils/constants.dart';
import 'package:luvit_assesment/home_page.dart';
import 'package:persistent_bottom_nav_bar/persistent_tab_view.dart';
import 'package:sizer/sizer.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  final scrollController = ScrollController();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.black,
      body: buildBottomAppBar(),
      floatingActionButton: Container(
        margin: EdgeInsets.only(bottom: 1.w),
        child: CircleAvatar(
          radius: 6.w,
          backgroundColor: Colors.grey,
          child: Padding(
            padding: const EdgeInsets.all(1.0),
            child: CircleAvatar(
              radius: 5.8.w,
              backgroundColor: CustomColors.secondgrey,
              child: const Icon(
                CupertinoIcons.star_fill,
                color: CustomColors.black,
              ),
            ),
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  List<Widget> _buildScreens() {
    return [
      const Homepage(),
      const Homepage(),
      const Homepage(),
      const Homepage(),
      const Homepage()
    ];
  }

  List<PersistentBottomNavBarItem> _navBarsItems() {
    return [
      PersistentBottomNavBarItem(
        icon: const Icon(Icons.home_rounded),
        title: ("홈"),
        activeColorPrimary: CustomColors.red,
        inactiveColorPrimary: CustomColors.secondgrey,
      ),
      PersistentBottomNavBarItem(
        icon: const Icon(Icons.location_on_outlined),
        title: ("스팟"),
        activeColorPrimary: CustomColors.red,
        inactiveColorPrimary: CustomColors.secondgrey,
      ),
      PersistentBottomNavBarItem(
          icon: Container(),
          activeColorPrimary: CustomColors.secondblack.withOpacity(0.9),
          inactiveColorSecondary: CustomColors.secondgrey,
          inactiveColorPrimary: CustomColors.secondgrey,
          activeColorSecondary: CustomColors.red),
      PersistentBottomNavBarItem(
        icon: const Icon(CupertinoIcons.chat_bubble_text),
        title: ("채팅"),
        activeColorPrimary: CustomColors.red,
        inactiveColorPrimary: CustomColors.secondgrey,
      ),
      PersistentBottomNavBarItem(
        icon: const Icon(CupertinoIcons.person_solid),
        title: ("마이"),
        activeColorPrimary: CustomColors.red,
        inactiveColorPrimary: CustomColors.secondgrey,
      ),
    ];
  }

  Widget buildBottomAppBar() {
    PersistentTabController controller;

    controller = PersistentTabController(initialIndex: 0);
    return PersistentTabView(context,
        padding: NavBarPadding.symmetric(horizontal: 0.w),
        controller: controller,
        screens: _buildScreens(),
        items: _navBarsItems(),
        confineInSafeArea: true,
        margin: EdgeInsets.symmetric(horizontal: 0.w),
        backgroundColor: CustomColors.black, // Default is Colors.white.
        handleAndroidBackButtonPress: true, // Default is true.
        resizeToAvoidBottomInset: true,
        stateManagement: true, // Default is true.
        hideNavigationBarWhenKeyboardShows: true,
        decoration: NavBarDecoration(
          borderRadius: BorderRadius.circular(10.0),
          border: const Border(top: BorderSide(color: Colors.grey)),
          colorBehindNavBar: CustomColors.white,
        ),
        popAllScreensOnTapOfSelectedTab: true,
        popActionScreens: PopActionScreensType.all,
        itemAnimationProperties: const ItemAnimationProperties(
          duration: Duration(milliseconds: 200),
          curve: Curves.ease,
        ),
        screenTransitionAnimation: const ScreenTransitionAnimation(
          animateTabTransition: true,
          curve: Curves.ease,
          duration: Duration(milliseconds: 200),
        ),
        navBarStyle: NavBarStyle.style6);
  }
}
